def firstFit(next_item):
    """
    Executes first fit for bin packing
    next_item   an iterator that should return new items of size <= 1
                corresponding to the items of the bin packing problem
    """
    # Initially 1 bin that is empty
    bins = [1]

    # Add nb_items
    for item in next_item():
        inserted = False
        # Insert in first bin where possible
        for i in range(len(bins)):
            if (bins[i] >= item) :
                bins[i] -= item
                inserted = True
                break
        # If still not inserted, open new bin and ad item to it
        if (inserted == False):
            bins.append(1-item)
    
    print("First fit used {} bins".format(len(bins)))
    return(len(bins))