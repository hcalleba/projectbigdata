# Project Big Data

Implemenatation of https://arxiv.org/pdf/1905.04897.pdf

## Running the algorithm

- python must be installed, the version we used was 3.8.10 sio any newer version should work.
- numpy must also be installed (pip install numpy)
- pyomo must be installed (pip install pyomo)
- glpk must be installed or any other MIP solver that works with pyomo.
    - if another solver is used, for example gurobi, then the SOLVER constant should be changed accordingly in the constants.py file
- the main.py file should then be run or alternatively, the comparisons.ipynb can be looked at as it is probably more meaningful than the plain main.py (note that the whole notebook probably takes about 30 minutes to run entirely as there are many tests)

To install glpk (on linux),  the following command should be run "sudo apt-get install glpk-utils libglpk-dev glpk-doc"

Alternatively, glpk can also be installed from sources and does not need to be on the PATH.
If it is not on the path, the two lines "pyo.SolverFactory(SOLVER)" in column_generation.py should be replaced by "pyo.SolverFactory('glpk', executable='path/to/solver/directory')"
