import pyomo.environ as pyo
from math import floor
from constants import SOLVER


# The code for column generation mainly comes from https://github.com/HismaelOliveira/ColumnGenerationCSP/blob/main/Cutting%20Stock%20Problem.ipynb
# but slightly modified
# See also https://github.com/scipopt/PySCIPOpt/blob/master/examples/unfinished/cutstock.py for another example ?


def master_problem(columns, item_amount, type=pyo.NonNegativeReals):
    """
    Creates the model for the master problem
    columns are the columns currently considered
    item_amount is a list containing the number of each item needed
    type must be pyo.NonNegativeReals or pyo.NonNegativeIntegers to indicate LP relxation or MIP problem
    returns the model
    """
    # Create model
    model = pyo.ConcreteModel()

    # Set of indices over the N items
    model.N = pyo.RangeSet(len(item_amount))
    # Set of indices over the number of columns
    model.J = pyo.Set(initialize=columns)

    # Create the x variables (number of vars = |J|)
    model.X = pyo.Var(model.J, within=type)

    # Create the objective of the LP
    def objectiveRule(model):
        return pyo.summation(model.X)
    model.OBJ = pyo.Objective(rule=objectiveRule, sense=pyo.minimize)

    # Create the (unique) constraint
    def constraintRule(model, i):
        return sum(j[i-1] * model.X[j] for j in model.J) >= item_amount[i-1]
    model.cm = pyo.Constraint(model.N, rule=constraintRule)

    # Let the model know that we will ask for the dual variables later
    model.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT)

    return model

def master_lp(columns, item_amount):
    """
    Creates the linear relaxation of the master problem
    """
    return master_problem(columns, item_amount, pyo.NonNegativeReals)

def master_mip(columns, item_amount):
    """
    Creates the Mixed Integer Program of the master problem
    """
    return master_problem(columns, item_amount, pyo.NonNegativeIntegers)

def slave_problem(duals, item_sizes):
    """
    Creates the model for the slave problem
    duals is the vector of optimal dual values obtained with the LP relaxation of the master problem
    item_sizes is a list containing the sizes of each item in the original bin packing problem
    returns the model
    """
    # Create model
    model = pyo.ConcreteModel()

    # Set of indices over the N items
    model.N = pyo.RangeSet(len(item_sizes))
    # Dual variables (lambda) as parameters
    model.L = pyo.Param(model.N, initialize=lambda model, i: duals[i-1])
    # Item sizes as parameters
    model.S = pyo.Param(model.N, initialize=lambda model, i: item_sizes[i-1])
    
    # Create the y variables
    model.Y = pyo.Var(model.N, within=pyo.NonNegativeIntegers)

    # Create the objective of the LP
    def objectiveRule(model):
        return (1 - pyo.summation(model.L, model.Y))
    model.OBJ = pyo.Objective(rule=objectiveRule, sense=pyo.minimize)

    # Create the constraint
    def constraintRule(model):
        return pyo.summation(model.S, model.Y) <= 1
    model.cs = pyo.Constraint(rule=constraintRule)

    return model

def init_weights(item_sizes):
    """
    Creates a set of columns to find a solution to the master problem
    returns the initial set of columns
    """
    return [[0 if i != j else floor(1/item_sizes[i]) for i in range(len(item_sizes))] for j in range(len(item_sizes))]

def CG_CSP(item_sizes, item_amount):
    """
    Function that combines the master and slave problem and computes the linear relaxation of
    the Gilmore-Gomory linear model for the cutting stock problem
    item_sizes is a list containing the sizes of each item in the original bin packing problem
    item_amount is a list containing the number of each item needed
    """
    columns = init_weights(item_sizes)

    opt = pyo.SolverFactory(SOLVER)

    while (True):
        # Solve master problem
        m_model = master_lp(columns, item_amount)
        sol = opt.solve(m_model)

        # Get dual variables
        dual_var = []
        for c in m_model.component_objects(pyo.Constraint, active=True):
            for index in c:
                dual_var.append(m_model.dual[c[index]])
        
        # Solve slave problem
        s_model = slave_problem(dual_var, item_sizes)
        sol = opt.solve(s_model)

        # If reduced costs are negative, then break otherwise add new column
        if (pyo.value(s_model.OBJ) > -0.0000001):  # This should ideally be >= 0, but i've had a problem where instead of 0 I had -7.993605777301127e-15 (because of precision) and the program then got stuck in a loop
            break
        else:
            columns.append([pyo.value(s_model.Y[i]) for i in s_model.Y])
    
    return [pyo.value(m_model.X[i]) for i in m_model.X], [i for i in m_model.X]