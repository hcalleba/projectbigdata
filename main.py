import random
from FirstFit import firstFit
from streaming_BP import simpleRounding, improvedRounding
import tracemalloc

def createIterator(nb_items=100000, seed=12345):
    def next_item():
        random.seed(seed)
        for _ in range(nb_items):
            yield random.random()
    return next_item

if (__name__=="__main__"):

    print("Hello :)")
    
    tracemalloc.start()
    firstFit(createIterator(10000))
    print(tracemalloc.get_traced_memory())
    tracemalloc.stop()

    tracemalloc.start()
    simpleRounding(createIterator(100000), epsilon=0.2)
    print(tracemalloc.get_traced_memory())
    tracemalloc.stop()

    tracemalloc.start()
    improvedRounding(createIterator(100000), epsilon=0.2)
    print(tracemalloc.get_traced_memory())
    tracemalloc.stop()
