from math import ceil, floor, log2
from gkarray import GKArray
from column_generation import *

def addSmallItems(item_sizes, cut_patterns, number_pattern, small_items_size, epsilon):
    """
    Fills a solution of the rounded BP instance with the small items
    item_sizes:         the sizes of each item in the Rounded instance
    cut_patters:        the patterns to be used in the solution of the rounded instance
    number_patter:      for each pattern the number of times that pattern is used
    small_items_size:   the total size of all small items
    epsilon:            the desired accuracy that was used in the previous problems
    """
    nb_bins_used = sum(ceil(i) for i in number_pattern)
    print("I_R used {} bins".format(nb_bins_used))

    nb_items = len(item_sizes)
    # Available space over all cuts and patters >= 0
    available_space = 0
    for i in range(len(cut_patterns)):
        if (number_pattern[i] > 0):
            # Space taken by the current pattern (0 < pattern_space <= 1)
            pattern_space = 0
            for j in range(nb_items):
                pattern_space += cut_patterns[i][j] * item_sizes[j]
            # Fill with the patterns that are entirely used
            available_space += floor(number_pattern[i]) * max(0, 1-pattern_space-epsilon)
            # If a pattern is only partially used, we know that the unused part is free
            portion = number_pattern[i] - floor(number_pattern[i])
            if (portion > 0):
                available_space += ceil(portion) * max(0, 1-pattern_space*portion-epsilon)
    small_items_size -= available_space
    if (small_items_size > 0):
        nb_bins_used += ceil(small_items_size/(1-epsilon))
    print("Instance used {} bins in total".format(nb_bins_used))
    return nb_bins_used

def simpleRoundingModel(next_item, epsilon):
    """
    Creates the constant space [O(1/epsilon^2)] rounded model of a BP problem defined by next_item with accuracy epsilon
    next_item:  an iterator that should return new items of size <= 1
                corresponding to the items of the bin packing problem
    epsilon:    the desired accuracy
    """
    delta = 1/4*epsilon**2
    # Create quantile summary
    quantiles_summary = GKArray(delta)
    small_items_size = 0

    # Read nb_items
    for item in next_item():
        # Count size of small items
        if (item <= epsilon):
            small_items_size += item
        # Add big items to the quantile summary
        else:
            quantiles_summary.add(item)
    # Process all buffered items of the quantile summary
    quantiles_summary.merge_compress()

    # Extract info from quantile summary and build High-Multiplicity BP problem
    item_sizes = []
    item_amount = []
    for i in quantiles_summary.entries:
        item_sizes.append(i.val)
        item_amount.append(i.g)

    print("High multiplicity bin packing problem (aka cutting stock problem) created with {} different item sizes".format(len(item_sizes)))

    return item_sizes, item_amount, small_items_size

def simpleRounding(next_item, epsilon):
    """
    Solves the BP problem with a rounded model given by improvedRoundingModel
    next_item:  an iterator that should return new items of size <= 1
                corresponding to the items of the bin packing problem
    epsilon:    the desired accuracy
    """
    # Create the simply rounded model
    item_sizes, item_amount, small_items_size = simpleRoundingModel(next_item, epsilon)
    # Do column generation on the rounded model
    val, col = CG_CSP(item_sizes, item_amount)
    # Add the small items to the solution
    return addSmallItems(item_sizes, col, val, small_items_size, epsilon)

def improvedRoundingModel(next_item, epsilon):
    """
    Creates the constant space [O(1/epsilon)] rounded model of a BP problem defined by next_item with accuracy epsilon
    next_item:  an iterator that should return new items of size <= 1
                corresponding to the items of the bin packing problem
    epsilon:    the desired accuracy
    """
    delta = 1/8*epsilon
    # Create list of quantile summaries ranging from [1.0, 0.5), [0.5, 0.25), [0.25, 0.125), ...
    quantiles_summaries = [GKArray(delta) for _ in range(ceil(-log2(epsilon)))]
    small_items_size = 0

    # Read nb_items
    for item in next_item():
        # Count size of small items
        if (item <= epsilon):
            small_items_size += item
        # Add big items to the quantile summary
        else:
            index = floor(-log2(item))
            quantiles_summaries[index].add(item)
    # Process all buffered items of the quantile summary
    for quantiles_summary in quantiles_summaries :
        quantiles_summary.merge_compress()

    # Extract info from quantile summary and build High-Multiplicity BP problem
    item_sizes = []
    item_amount = []
    for quantiles_summary in quantiles_summaries : 
        for i in quantiles_summary.entries:
            item_sizes.append(i.val)
            item_amount.append(i.g)

    print("High multiplicity bin packing problem (aka cutting stock problem) created with {} different item sizes".format(len(item_sizes)))

    return item_sizes, item_amount, small_items_size

def improvedRounding(next_item, epsilon):
    """
    Solves the BP problem with a rounded model given by improvedRoundingModel
    next_item:  an iterator that should return new items of size <= 1
                corresponding to the items of the bin packing problem
    epsilon:    the desired accuracy
    """
    # Create the rounded model
    item_sizes, item_amount, small_items_size = improvedRoundingModel(next_item, epsilon)
    # Do column generation on the rounded model 
    val, col = CG_CSP(item_sizes, item_amount)
    # Add the small items to the solution
    return addSmallItems(item_sizes, col, val, small_items_size, epsilon)